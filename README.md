# Wardrobify

Team:

* Justin Huh - Hats microservice
* Skylar Stout - Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

Install Django app
Create endpoint in urls.py
Create Model for shoes (manufacturer, model name, color, href, bin)
Create a view to get a list of shoes using the Model
Configure the views in urls.py
Include URL's from app to project URL's
Set up insomnia
Build React fetch component for list/show list of shoes
Nav bar to include functional path
Create functional poller to get Bin/Location data
Create functional delete button
## Hats microservice

Creating a hat model that includes: fabric, style_name, color, URL for picture, and location in wardrobe where it exists.

Creating a view function to show my model.
Creating pathways for my URLs - endpoints.

Set up Insomnia - requests.

Build React component to fetch and show hat list. Link it to Nav bar.

Create poller to get Bin/Location data.

Create delete function: delete button for each item in my hat list.
