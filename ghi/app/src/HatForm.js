import React, { useState, useEffect } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        style_name: '',
        fabric: '',
        color: '',
        location: '',
        picture_url: '',
    })

    const [hasCreated, setHasCreated] = useState(false)

    const getData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = `http://localhost:8090/api/hats/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                style_name: '',
                fabric: '',
                color: '',
                location: '',
                picture_url: '',
            })

            setHasCreated(true);
        };
    }

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width="300"
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/pughat.jpeg"
                        alt='Logo'
                    />
                </div>

                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">

                            <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form">
                                <h1 className="card-title">Hat Creation</h1>
                                <p className="mb-3">
                                    Please choose where you'd like to store your hat.
                                </p>

                                <div className="mb-3">
                                    <select onChange={handleChangeName} name="location" id="location" required>
                                        <option value="">Choose location</option>
                                        {
                                            locations.map(location => {
                                                return (
                                                    <option key={location.id} value={location.href}>{location.closet_name}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>

                                <p className="mb-3">
                                    Input the hat details.
                                </p>

                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Style Name" type="text" id="style_name" name="style_name" className="form-control" />
                                            <label htmlFor="style_name">Style Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Type of Fabric" type="text" id="fabric" name="fabric" className="form-control" />
                                            <label htmlFor="fabric">Type of Fabric</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="color">Color</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <div class="form-group">
                                        <label htmlFor="picture_url"></label>
                                        <input onChange={handleChangeName} required placeholder="Picture URL" type="URL" id="picture_url" name="picture_url" className="form-control" />
                                    </div>
                                </div>

                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>

                            <div className={messageClasses} id="success-message">
                                Created Hat!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );



}

export default HatForm
