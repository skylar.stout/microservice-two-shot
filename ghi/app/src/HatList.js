import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
            console.log("data:", data)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const deleteHat = async (id) => {
        const url = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, fetchConfig);
        getData();
    };

    return (
        <>
            <div className="mb-3 mt-3 d-grid gap-2 d-md-flex justify-content-md-end">
                <Link to="/hats/create" className="btn btn-primary btn-lg px-4 gap-3">
                    Create Hat
                </Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Style Name</th>
                        <th>Location</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key={hat.id}>
                                <td>
                                    <img
                                        className="img-thumbnail"
                                        src={hat.picture_url}
                                        alt={hat.style_name}
                                        style={{ width: '50px' }}
                                    />
                                </td>
                                <td>{hat.style_name}</td>
                                <td>{hat.location}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.color}</td>
                                <td>
                                    <button onClick={() => deleteHat(hat.id)} type="button" id={hat.id} className="btn btn-danger">Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table >
        </>
    );
}

export default HatsList;
