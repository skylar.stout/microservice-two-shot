import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import ShoesAdd from './ShoesAdd';
import ShoesList from './ShoesList';
import Nav from './Nav';
import HatsList from './HatList';
import HatForm from './HatForm';

function App() {
  const [shoes, setShoes] = useState([]);

  async function getShoes() {
      const url = 'http://localhost:8080/api/shoe_list/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setShoes(data.Shoes);
      } else {
        console.error('Error fetching data')
      }
    }


    useEffect(() => {
      getShoes();
    }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/" element={<HatsList />} />
          <Route path="hats/create" element={<HatForm />} />
          <Route path="/shoes" element={<ShoesList shoes={shoes}/>} />
          <Route path="/shoes_add" element={<ShoesAdd />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
