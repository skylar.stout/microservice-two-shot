import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function ShoesList(props) {
  async function getID(event) {
    const url = `http://localhost:8080/api/shoe_rest/${event.target.id}`;
    console.log(url);
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    await fetch(url, fetchConfig);
    window.location.reload(false);
  }
  return (
    <>
      <div className="mb-3 mt-3 d-grid gap-2 d-md-flex justify-content-md-end">
        <Link to="/shoes_add" className="btn btn-primary btn-lg px-4 gap-3">
          Create Shoe
        </Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>
                  <img height={"50px"} width={"50px"} src={shoe.picture} />
                </td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>{shoe.bin.closet_name}</td>
                <td>
                  <button
                    onClick={getID}
                    type="button"
                    id={shoe.id}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ShoesList;
