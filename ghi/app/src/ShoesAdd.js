import React, { useState, useEffect } from 'react';


function ShoesAdd({ }) {
  const [manufacturer, setManufacturer] = useState('');
  const [model_name, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [picture, setPicture] = useState('');
  const [bin, setBin] = useState('');
  const [bins, setBins] = useState([]);

  async function fetchBins() {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchBins();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      manufacturer,
      model_name,
      color,
      picture,
      bin,
    };
    console.log(data)
    const shoeUrl = 'http://localhost:8080/api/shoe_list/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      setManufacturer('');
      setModelName('');
      setColor('');
      setPicture('');
      setBin('');
    }
  }

  function handleChangeManufacturer(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  function handleChangeModelName(event) {
    const { value } = event.target;
    setModelName(value);
  }

  function handleChangeColors(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handleChangePicture(event) {
    const { value } = event.target;
    setPicture(value);
  }

  function handleChangeBin(event) {
    const { value } = event.target;
    console.log(value)
    setBin(value);
  }

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                value={manufacturer}
                onChange={handleChangeManufacturer}
                placeholder="Manufacturer"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="name">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={model_name}
                onChange={handleChangeModelName}
                placeholder="Name of shoe"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="starts">Name of Shoe</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={color}
                onChange={handleChangeColors}
                placeholder="Colors"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="ends">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={picture}
                onChange={handleChangePicture}
                placeholder="Picture"
                required
                type="text"
                name="picture"
                id="picture"
                className="form-control"
              />
              <label htmlFor="ends">Picture</label>
            </div>
            <div className="mb-3">
              <select
                value={bin}
                onChange={handleChangeBin}
                required
                name="bin"
                id="bin"
                className="form-select">
                <option value="">Choose a bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.id} value={bin.href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoesAdd;
