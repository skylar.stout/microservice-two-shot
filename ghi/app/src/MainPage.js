import { Link } from "react-router-dom";

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have the solution for you!
        </p>
      </div>

      <div className="container my-5">
        <div className="row">
          {/* For Hats */}
          <div className="col-md-6 mb-4">
            <Link to="/hats">
              <div className="bg-image position-relative rounded">
                <img src="/hat-background.jpg" className="w-100 img-fluid rounded-circle" alt="hats" />
                <div className="mask" style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)' }}></div>
                <div className="d-flex justify-content-center align-items-center text-white position-absolute top-50 start-50 translate-middle">
                  <p className="text-center mb-0" style={{ fontSize: '2rem', textShadow: '2px 2px 2px black' }} >HATS</p>
                </div>
              </div>
            </Link>
          </div>
          {/* For Shoes */}
          <div className="col-md-6 mb-4">
            <Link to="/shoes">
              <div className="bg-image position-relative rounded">
                <img src="/shoes-background.jpg" className="w-75 img-fluid rounded-circle" alt="shoes" />
                <div className="mask" style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)' }}></div>
                <div className="d-flex justify-content-center align-items-center text-white position-absolute top-50 start-50 translate-middle">
                  <p className="text-center mb-0" style={{ fontSize: '2rem', textShadow: '2px 2px 2px black' }} >SHOES</p>
                </div>
              </div>
            </Link>
          </div>

        </div>
      </div>
    </div>
  );
}

export default MainPage;
