from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
import json
from .models import Shoe, BinVO
from common.json import ModelEncoder

# Create your views here.


class BinVODetails(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name"]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVODetails(),
    }


@require_http_methods(["GET", "POST"])
def shoe_list(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            Shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            Shoes = Shoe.objects.all()
            return JsonResponse(
                {"Shoes": Shoes},
                encoder=ShoeDetailEncoder,
            )
    else:
        content = json.loads(request.body)

        # Get the bin object and put it in the content dict
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        new_shoe = Shoe.objects.create(**content)
        return JsonResponse(
            new_shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def shoe_delete(request, id):
    Shoe.objects.filter(id=id).delete()
    return JsonResponse(
            {"message": ""},
            status=400,
        )
