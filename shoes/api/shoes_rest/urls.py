from django.urls import path

from .views import shoe_list, shoe_delete

urlpatterns = [
    path("shoe_list/", shoe_list, name="api_create_shoe"),
    path("shoe_rest/<int:id>/", shoe_delete, name="api_delete_shoe"),
]
